using UnityEngine;

public class Bonus : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;

    [HideInInspector] public Vector3 playerPosition;
    public bool stopped;
    
    private Vector3 _guidanceSpeedAddition;

    private void FixedUpdate()
    {
        if(stopped) return;
        if (Vector3.Distance(playerPosition, transform.position) <= gameSettings.bonusReactionRadius)
            _guidanceSpeedAddition += gameSettings.bonusAcceleration * (playerPosition - transform.position).normalized;
        else _guidanceSpeedAddition = Vector3.zero;

        transform.Translate(  Vector3.ClampMagnitude(_guidanceSpeedAddition, gameSettings.bonusMaxSpeed)* Time.fixedDeltaTime);
    }
}