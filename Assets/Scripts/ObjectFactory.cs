using System;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObjectFactory : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;
    [SerializeField] private Bonus bonusPrefab;
    [SerializeField] private OscillateY obstaclePrefab;
    [SerializeField] private Transform player;
    
    public bool isStopped;

    private GameObjectPool<OscillateY> _obstaclePool;
    private GameObjectPool<Bonus> _bonusPool;
    private float _groupDistance;
    private float _bonusDistance;
    private float _groupDistanceTarget;
    private float _bonusDistanceTarget;

    private void Start()
    {
        _groupDistanceTarget = Random.Range(gameSettings.groupDistanceMin, gameSettings.groupDistanceMax);
        _bonusDistanceTarget = Random.Range(gameSettings.bonusDistanceMin, gameSettings.bonusDistanceMax);
        _obstaclePool = new GameObjectPool<OscillateY>(obstaclePrefab);
        _bonusPool = new GameObjectPool<Bonus>(bonusPrefab);
    }

    private void Update()
    {
        if(isStopped) return;
        if (_groupDistance < _groupDistanceTarget)
            _groupDistance += gameSettings.gameSpeed * Time.deltaTime;
        else
        {
            _groupDistance = 0;

            int thisGroupCount = Random.Range(1, gameSettings.maxGroupNumber+1);

            for (int i = 1; i <= thisGroupCount; i++)
            {
                Vector3 spawnPosition = new Vector3(
                    Random.Range(gameSettings.spawnXCoord, gameSettings.spawnXCoord + gameSettings.maxGroupWidth),
                    Random.Range(gameSettings.minSpawnHeight, gameSettings.maxSpawnHeight), 0);
                _obstaclePool.GetOne(spawnPosition, Quaternion.identity);
            }
            _groupDistanceTarget = Random.Range(gameSettings.groupDistanceMin, gameSettings.groupDistanceMax);
        }
        
        if (_bonusDistance < _bonusDistanceTarget)
            _bonusDistance += gameSettings.gameSpeed * Time.deltaTime;
        else
        {
            _bonusDistance = 0;

            Vector3 spawnPosition = new Vector3( gameSettings.spawnXCoord,
                Random.Range(gameSettings.minSpawnHeight, gameSettings.maxSpawnHeight), 0);

             _bonusPool.GetOne(spawnPosition, Quaternion.identity);
             _bonusDistanceTarget = Random.Range(gameSettings.bonusDistanceMin, gameSettings.bonusDistanceMax);
        }

    }

    private void FixedUpdate()
    {
        foreach (Bonus bonus in _bonusPool.AllObjects) bonus.playerPosition = player.position;
    }
}
