using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;
    [SerializeField] private Player player;
    [SerializeField] private TrailMaker trailMaker;
    [SerializeField] private TextMeshProUGUI scoreCounter;
    [SerializeField] private ObjectFactory factory;
    [SerializeField] private GameObject starterMessage;

    private int _score;
    private GameState _gameState;
    private float _speed;

    private void Start()
    {
        player.Set(()=> _speed, PlayerCollision);
    }

    private void Update()
    {
        switch (_gameState)
        {
            case GameState.Starting:
                if (ControlPressed())
                {
                    _gameState = GameState.Playing;
                    starterMessage.SetActive(false);
                    trailMaker.Play();
                    factory.isStopped = false;
                }
                break;
            case GameState.Playing:
                UpdatePlayerSpeed();
                break;
            case GameState.Fnishing:
               SceneManager.LoadScene(SceneUtility.GetScenePathByBuildIndex(0)); //a tiny bit of shitcode
               break;
        }
    }

    private void UpdatePlayerSpeed() => 
        _speed += ControlPressed()?  gameSettings.upAcceleration*Time.deltaTime: 
            -gameSettings.downAcceleration * Time.deltaTime;

    private bool ControlPressed() => Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0) || Input.touchSupported && Input.touchCount > 0;

    public void PlayerCollision(Collision2D collision)
    {
        if (collision.collider.TryGetComponent(out Bonus _))
        {
            _score++;
            if (scoreCounter) scoreCounter.text = _score.ToString();
            collision.collider.gameObject.SetActive(false);
            return;
        }

        
        _gameState = GameState.Fnishing;
        trailMaker.Pause();
    }
    
    private enum GameState
    {
        Starting,
        Playing,
        Fnishing
    }
}