using System;
using System.Diagnostics;
using UnityEngine;
using Random = UnityEngine.Random;


public class OscillateY : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;

    public bool isStopped;

    private float _originalPositionY;
    private float _timer;
    private float _hoverTime;
    private Vector3 _speedDirection;

    private void OnEnable()
    {
        _originalPositionY = transform.position.y;
        _speedDirection = Mathf.Sign(-0.5f + UnityEngine.Random.Range(0, 1)) * Vector3.up;
        _hoverTime = Random.Range(0, gameSettings.obstacleMaxPauseTime);
    }

    private void FixedUpdate()
    {
        if (isStopped) return;
        if (transform.position.y >= _originalPositionY + gameSettings.obstacleOscillationDistance / 2)
        {
            if (_timer < _hoverTime)
            {
                _timer += Time.fixedDeltaTime;
                return;
            }

            _speedDirection = Vector3.down;
            _timer = 0;
        }

        if (transform.position.y <= _originalPositionY - gameSettings.obstacleOscillationDistance / 2)
        {
            if (_timer < _hoverTime)
            {
                _timer += Time.fixedDeltaTime;
                return;
            }

            _speedDirection = Vector3.up;
            _timer = 0;
        }

        transform.Translate(gameSettings.obstacleOscillationSpeed * Time.fixedDeltaTime * _speedDirection);
    }
}
    
