using UnityEngine;

[CreateAssetMenu(menuName = "game settings")]
public class GameSettings : ScriptableObject
{
    [Header("Player")]
    public float gameSpeed;
    public float upAcceleration;
    public float downAcceleration;

    [Header("Generator")] 
    public float groupDistanceMin;
    public float groupDistanceMax;
    public float bonusDistanceMin;
    public float bonusDistanceMax;
    public float maxGroupWidth;
    public float minSpawnHeight;
    public float maxSpawnHeight;
    public float spawnXCoord;
    public int maxGroupNumber;
    public float disableXCoord;

    [Header("Obstacles")]
    public float obstacleOscillationSpeed;
    public float obstacleOscillationDistance;
    public float obstacleMaxPauseTime;

    [Header("Bonuses")] 
    public float bonusAcceleration;
    public float bonusMaxSpeed;
    public float bonusReactionRadius;
}
