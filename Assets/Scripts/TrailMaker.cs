using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class TrailMaker : MonoBehaviour
{
    [SerializeField] private GameSettings settings;

    private ParticleSystem _particleSystem;

    private void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
        var main = _particleSystem.main;
        main.startSpeed = settings.gameSpeed;
        Pause();
    }

    public void Play() => _particleSystem.Play();
    public void Pause() => _particleSystem.Pause();
}
