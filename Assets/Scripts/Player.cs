using System;
using JetBrains.Annotations;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool isSet;
    
    private Func<float> _speed;

    private Action<Collision2D> _playerCollided;

    private void FixedUpdate()
    {
        if(!isSet) return;
        transform.Translate(Vector3.up * _speed() *Time.fixedDeltaTime);
    }

    public void Set([CanBeNull] Func<float> speedMethod, Action<Collision2D> collisionCallback)
    {
        _playerCollided = collisionCallback;
        _speed = speedMethod;
        isSet = true;
    }

    private void OnCollisionEnter2D(Collision2D col) => _playerCollided?.Invoke(col);
}

