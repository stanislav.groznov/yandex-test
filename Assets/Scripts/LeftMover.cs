using System;
using Unity.VisualScripting;
using UnityEngine;

public class LeftMover : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;

    public bool isStopped;

    private void FixedUpdate()
    {
       if( isStopped) return;
       if(transform.position.x <= gameSettings.disableXCoord) gameObject.SetActive(false);
       transform.Translate(gameSettings.gameSpeed *Time.fixedDeltaTime* Vector3.left);
    }
}